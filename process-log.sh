dir=$1
complete_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/$dir" # https://stackoverflow.com/a/246128
winpty -Xallow-non-tty ~/AppData/Local/Programs/Python/Python38/python.exe ~/Documents/Paradox\ Interactive/Hearts\ of\ Iron\ IV/mod/git-days-of-europe/useful_python_programs/USPresidentialElectionSimulatorGamelogValidator.py $complete_dir
success=$?
if [ $success -ne 0 ]; then
	read -p "game.log may be broken, press c to continue anyway: " input
	if [ $input == "c" ]; then
		winpty -Xallow-non-tty ~/AppData/Local/Programs/Python/Python38/python.exe ~/Documents/Paradox\ Interactive/Hearts\ of\ Iron\ IV/mod/git-days-of-europe/useful_python_programs/USPresidentialElectionSimulatorUnifiedCLI.py $complete_dir
		timestamp=$(date +%Y-%m-%d_%H-%M-%S)
		git add .
		git commit -m "Automated commit of new election data" -m "Timestamp: ${timestamp}" -m "Game: ${dir}"
		git push origin master
	fi
fi